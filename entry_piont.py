# -*- coding: utf-8 -*-

import os
import sys
import time

import argparse

from logging import INFO
from utils import enter_exit, create_logger
from inputs import get_configs_dict, get_inputs,get_inputs_GUI
from app import email_discombobulate

logger = create_logger("EdtPIIDetector",INFO)



configs_mapping = {
  "MetaDataCsvFile": str(),
  "TextFilesFolder": str()
}


version = '0.0.1'

doc = """The utility uses natural language processing and machine learning to 
automatically identify PII candidates from the party information.
"""

trial_exp_date = '2019-08-01-12-00'

footnote = """NOTE: The evaluation period for this utility expires on 1 Feb 2019.
Please email support@discoveredt.com to provide feedback or to obtain 
support.
"""

interactive_msg = """

_________________________________________
       EDT AI Lab: Dev Prototypes        
_________________________________________
           PII Detector (PIID)           
_________________________________________

Loading environment and modules, please wait...


"""


def create_parser():
    """Return command-line parser"""
    parser = argparse.ArgumentParser(description=doc,
                                     epilog=footnote,
                                     prog='EdtPIIDetector')
    
    parser.add_argument('--version', action='version',
                        version=version)
    parser.add_argument('-c', '--use-configs', action='store_const',
                        const=True,
                        help='set this flag to use configs file; '
                             'if used, other flags --data-directory'
                             ' will be ignored')
    parser.add_argument('-e', '--email-only', action='store_const',
                        const=True,
                        help='set this flag to skip filtering data-directory; '
                             'if used, the algorithem assums that data-directory'
                             ' contains only emails text files '
                             'and the metadata file will be ignored')
    parser.add_argument('-f', '--configs-file', action='store',
                        default='configs/input.json', metavar='filepath',
                        help='path to configuration file, only used if '
                             '--use-configs is set (default: '
                             'configs/input.json)')
    parser.add_argument('-d', '--data-directory', action='store', 
                        metavar='filepath',
                        help='path to directory that includes all emails text files'
                            'where filename is equal to Document ID in the metadata file'
                            ' mentioned in -m argument.'
                            'ignored if --use-configs is set')
    parser.add_argument('-m', '--metadata-file', action='store', 
                        metavar='filepath',
                        help='path to metadata csv file that Document ID and '
                        'Document Type (or Document Kind). The file is used to '
                        'select email documents only for processing from'
                        ' directory mentioned in -d argument.'
                        'ignored if --use-configs is set')

    return parser


def trial_check():
    logger.info(interactive_msg)
    time.sleep(3)
    _ = os.system('cls')
    if time.strftime("%Y-%m-%d-%H-%M") <= trial_exp_date:
       pass
    else:
       logger.info(
         "Your EdtAliasDetector trial has expired! Please contact "
         "info@discoveredt.com for more information.")
       enter_exit(1)


def parse_args(arguments):
    """Parse command-line options"""
    parser = create_parser()
    args = parser.parse_args(arguments)
    inputs_dict = None
    
    if args.use_configs:
      if args.configs_file:
        inputs_dict = get_configs_dict(args.configs_file, configs_mapping)
      else:
        parser.error(
        "--configs-file should be provided if --use-configs flag is set")
    
    elif args.data_directory and (args.metadata_file or args.email_only):
       inputs_dict = {'MetaDataCsvFile': args.metadata_file,
                      'TextFilesFolder': args.data_directory}
    else:
       parser.error(
        "please use --cofigs-file option along with --use-configs "
        "or set both --data-directory and --metadata-file")
    if args.email_only:
      inputs_dict['EmailsOnly']= args.email_only
    return inputs_dict


def main():
    """Command-line entry"""
    trial_check()
    logger.info("Program Start") 
    inputs_dict = None
    if len(sys.argv) > 1:
      inputs_dict = parse_args(sys.argv[1:])
    else:
      inputs_dict = get_inputs_GUI()
    if inputs_dict is None:
        raise RuntimeError(
            "Failed to get required user inputs needed to run the utility")
    emails_files, inputs_directory = get_inputs(inputs_dict) #TODO validate input dic values

    #------------------------------------------------------
    #  Create Output directory
    #------------------------------------------------------
    
    
    outdir = os.path.join("output", time.strftime("%Y-%m-%d_%H-%M"))
    
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
        os.makedirs(os.path.join(outdir, "emails"))
                    
    logger.info(
        "Creating output files at %s..."
        % outdir
        )
    #_ = email_discombobulate(emails_files,inputs_directory, outdir+'/',logger,'utf-16')
    
    return 0


if __name__ == '__main__':
   try:
      
      enter_exit(main())
   
   except Exception as e:
      
      if isinstance(e, RuntimeError): 
        logger.error("%s: %s" % (type(e).__name__, e))

        logger.exception(e)
      else:
         logger.info("%s: %s" % (type(e).__name__, e))
         logger.debug(e)
      
      enter_exit(1)
   print("is running")
