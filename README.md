### Profanity detector ###

Project to check the occurances of profane words in the input text.

Steps to follow:

create the virtual environment with command  - virtualenv virtualenvname

activate the virtual env using the command - source venv/bin/activate

install necessory python libraries using the command - pip install -r requirements.txt

run the project with the command - python demo.py