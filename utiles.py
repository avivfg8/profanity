import enchant
import json
import string
import nltk
from nltk.tokenize import regexp_tokenize # , RegexpTokenizer
from nltk.corpus import stopwords, words
import re
from fuzzywuzzy import fuzz,process
import difflib
import csv
import os

# from flask import Flask,request, render_template, jsonify, make_response, redirect, url_for
import sys
from collections import Counter



# app = Flask(__name__) # @aviv
#nltk.download()
file = open("bad_words.txt","r") # @aviv
profane_words = file.read().replace('\r\n','\n').split('\n')
#print("profane_words",profane_words) # @aviv
profane_word_found = []
spell_checker = enchant.Dict("en_US") # @aviv
caught_words = {}

def write_to_csv(caught_words):
	output_file = open("output.csv","w")
	csv_file = csv.writer(output_file)
	csv_file.writerow(["Profane words","Occurrences"])
	for pword in caught_words:
		occurrences = len(caught_words[pword])

		csv_file.writerow([pword,occurrences])
	output_file.close()
	caught_words.clear()

def word_found(word, closest_pword, ratio):
	if closest_pword in caught_words:
		if (word,ratio) not in caught_words[closest_pword]: # will be more helpful when we have found the same string in more than 1 place, and we save the match as (word,ratio,place_in_text)
			caught_words[closest_pword].append((word,ratio))
			return
	# else (that's the first time that this word was found. add it):
	caught_words[closest_pword] = [(word,ratio)]

def word_check(word, letters_percentage):
	# if len(word)>2: # this is really wrong if we have 2-letter words (such as: "FU") in our bad-of-words,
	# 			    #or even a 2-letters mistake that should have been a 3-letter bad word.
	# 	w = re.findall("[\!\#\$\*\@]+.*",word)
	# 	print("w is: ",w) # 
	# 	if w: # WRONG - this is checking if a word have one of the chars (1 line above), and throw away the rest. 
	closest_pword, pword_index = process.extractOne(word,profane_words, scorer = fuzz.ratio) # extracting the closest profane-word from the profane_words list.
	#we can change the metric by adding a scorer parameter: process.extractOne(word,profane_words,scorer=fuzz.some_optional_sort)

	# M = most words in the BOW.
	# If p_phrase consist more than 1 word, check first only the first word of the p_phrase.
	# If it is following some same attributes (such as length..), save it with the next M-1 words and wait for the next word. else, leave it...
	# After the lookup ends, check if the rest 

	fuzz_ratio = fuzz.ratio(word, closest_pword)
	print("fuzz.ratio("+word+","+closest_pword+") = ", fuzz_ratio)
	if fuzz_ratio > (69 * letters_percentage):
		if not word_diff(word,closest_pword):
			print("'",word, "' IS NOT A PROFANE WORD, BECAUSE OF word_diff")
			return False
		if fuzz_ratio < (91 * letters_percentage):
			if is_valid_english(word):
				print("'",word, "' IS NOT A PROFANE WORD, BECAUSE OF is_valid_english")
				return False
			else:
				word_found(word, closest_pword,fuzz_ratio) # this is for accepting a word that:
												    		# (1) is between 70% and 90% similar to a profane word, (and)
												    		# (2) the distance between them is not greater than 4, (and)
												    		# (3) is not a valid English one.
														   # maybe later we would want to put them into more tests, to assure that they perform profanity.
		else:
			word_found(word, closest_pword, fuzz_ratio) #  accepting words that scored a fuzz_ratio of 90% or above with their closest profane-word.
	else:
		print("'",word, "' IS NOT A PROFANE WORD, BECAUSE OF fuzz.ratio")
	return True

def preprocess(email_body):
	email_body = email_body.lower()
	# sentence = sentence.replace("@","a").replace("0","o").replace('\\xe9','') # muted by @aviv
	#tokenizer = RegexpTokenizer(r'[^,()\[\]\.\s\d]+')
	#tokens = tokenizer.tokenize(email_body)
	tokens = regexp_tokenize(email_body, pattern='\w+|\$[\d\.]+|\S+')
	filtered_words = [w for w in tokens if not w in stopwords.words('english')]
	return filtered_words


def is_valid_english(word):
	# return word in words.words() # @aviv
	#print("word is: ", word)
	#print("spell_checker.check(word) returns -  ",spell_checker.check(word))
	return spell_checker.check(word)

def word_diff(word,profane_word):
	diff = difflib.SequenceMatcher(None,word,profane_word)
	word_len = len(word)
	prof_len = len(profane_word)
	FIRST_CH_IDX = 0
	diff_size = diff.find_longest_match(FIRST_CH_IDX,word_len,FIRST_CH_IDX,prof_len).size
	# diff_size is the size of the biggest shared substring  (between word[1ST_CH_IDX:word_len] profane_word[1ST_CH_IDX:prof_len])
	max_len = max(word_len, prof_len)
	diff_of_words = abs(diff_size - max_len)
	print("diff_of_words(("+word+","+profane_word+")) = ", diff_of_words)
	if diff_of_words>4:
		return False
	return True











# 
# OLD USE OF FLASK - CONSIDER TO USE IT EVENTUALLY # @aviv
#


# @app.route("/",methods=['GET','POST'])
# def start():
# 	if request.method == "POST":
# 		msg = request.form.get('msg')
# 		words_list = preprocess(msg)

# 		for word in words_list:
# 			check = word_check(word)
# 			if check:
# 				continue
# 			profanity_test = process.extractOne(word,profane_words)
# 			if(profanity_test[1]>74):
# 				fuzz_ratio = fuzz.ratio(word, profanity_test[0])
# 				if fuzz_ratio>60:
# 					if not word_diff(word,profanity_test[0]):
# 						continue
# 					if(fuzz_ratio<91):
# 						if not is_valid_english(word):
# 							word_found(profanity_test[0])
# 							profane_word_found.append(word)
# 					else:
# 						word_found(profanity_test[0])
# 						profane_word_found.append(word)

# 		write_to_csv(caught_words)
# 		return jsonify({"message":"done"})
# 	else:
# 		return render_template('index.html',name='home')

# @app.route("/getPlotCsv",methods=['GET'])
# def getPlotCSV():
# 	if request.method == "GET":
# 		output = open('output.csv','r').read()
# 		response = make_response(output)
# 		cd = 'attachment; filename=mycsv.csv'
# 		response.headers['Content-Disposition'] = cd
# 		response.mimetype='text/csv'
# 		return response


# if __name__ == "__main__":
# 	app.run(host='0.0.0.0',debug=True,port=3003)









# Untill the code is ready we want to use this local run for debugging, and not the Flask (again -  for now).
# So we just ask for a file (which later will be given to us as the "msg" (see above, in the canceled "def start():"))

# getting the input:
def get_input():
	if len(sys.argv) == 1:
		print("No document recieved.\n\tYou can also give filename as a command line argument")
		filename = input("Enter Filename: ")
	else:
		filename = sys.argv[1]
	input_file = open(filename,"r")
	return input_file.read()


def longest_phrase_len(phrase_list):
	max_len = 0
	for phrase in phrase_list:
		length = len(phrase)
		if  length > max_len:
			max_len = length
	return max_len



	

if __name__ == "__main__":
	phrase_list = preprocess(get_input())
	print("words_list (from input) is:\n",phrase_list)



	longest_phrase_len = longest_phrase_len(phrase_list)



	for word in phrase_list:
		print("#######################\n THE WORD IS: '"+word+"'.")
		the_ABC = 'abcdefghijklmnopqrstuvwxyz'
		ABC_count = sum(word.count(letter) for letter in the_ABC)
		print("The amount of real letters in the word '"+word+"' is - ",ABC_count,".")
		letters_percentage = ABC_count/len(word)
		print('This word have ',100*letters_percentage,'%','of actual ABC-letters')

		if letters_percentage == 0:
			print('it is a total GRAWLIX!')
			print('add it')
			continue
		if letters_percentage == 1:
			print('no grawlix at all')

		check = word_check(word, letters_percentage)
	write_to_csv(caught_words)

